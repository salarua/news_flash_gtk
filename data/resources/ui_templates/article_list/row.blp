using Gtk 4.0;

template $ArticleRow : Box {
  height-request: 108;

  GestureClick row_click {
    button: 3;
  }

  GestureClick row_activate {
    button: 1;
  }

  GestureLongPress row_long_press {}

  Box {
    margin-top: 8;
    margin-bottom: 8;
    orientation: vertical;

    Box date_box {
      height-request: 24;

      Image favicon {
        halign: center;
        width-request: 16;
        height-request: 16;
        margin-end: 8;
        margin-top: 4;
        margin-bottom: 4;
        icon-name: "application-rss+xml-symbolic";
      }

      Label feed_label {
        hexpand: false;
        halign: start;
        valign: center;
        label: _("feed");
        wrap: true;
        wrap-mode: char;
        ellipsize: end;
        xalign: 0;

        styles [
          "subtitle",
        ]
      }

      Label {
        hexpand: false;
        halign: start;
        valign: center;
        label: _("/");
        xalign: 1;
        margin-start: 4;
        margin-end: 4;

        styles [
          "subtitle",
        ]
      }

      Label date_label {
        hexpand: false;
        halign: start;
        valign: center;
        label: _("date");
        xalign: 1;

        styles [
          "subtitle",
        ]
      }

      [end]
      $ArticleRowTags article_tags {}

      [end]
      Image marked {
        visible: false;
        halign: end;
        icon-name: "starred";
      }
    }

    Box {
      vexpand: true;

      Box {
        hexpand: true;
        orientation: vertical;

        Label title_label {
          halign: start;
          tooltip-text: _("title");
          label: _("title");
          wrap: true;
          wrap-mode: word_char;
          ellipsize: end;
          lines: 2;

          styles [
            "large",
          ]
        }

        Inscription summary_label {
          margin-top: 4;
          vexpand: true;
          text: _("No Summary");
          wrap-mode: word_char;
          text-overflow: ellipsize_end;
          min-lines: 1;

          styles [
            "subtitle",
          ]
        }
      }

      [end]
      Revealer thumb_revealer {
        transition-type: slide_right;
        transition-duration: 50;

        Picture thumb_image {
          margin-start: 8;
          can-shrink: true;
          content-fit: cover;
          valign: center;
          styles [
            "thumbnail",
          ]
        }
      }
    }
  }
}
