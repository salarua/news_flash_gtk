use thiserror::Error;

#[derive(Error, Debug)]
pub enum SettingsError {
    #[error("Failed to read or write config file on disk.")]
    IO(#[from] std::io::Error),
    #[error("Failed to serialize settings struct.")]
    Serialize(#[from] serde_json::Error),
    #[error("Keybind name not valid: {0}")]
    InvalidKeybind(String),
}
