use glib::Boxed;
use news_flash::ParsedUrl;

#[derive(Clone, Debug, Boxed)]
#[boxed_type(name = "NewsFlashGParsedUrl")]
pub struct GParsedUrl(ParsedUrl);

impl From<ParsedUrl> for GParsedUrl {
    fn from(pu: ParsedUrl) -> Self {
        Self(pu)
    }
}

impl From<GParsedUrl> for ParsedUrl {
    fn from(pu: GParsedUrl) -> Self {
        pu.0
    }
}
